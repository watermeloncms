<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title>Brak konfiguracji</title>
   <meta http-equiv="Pragma" content="no-cache, private">
   <meta http-equiv="Expires" content="0">
   <meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate">
   <meta http-equiv="Cache-Control" content="post-check=0, pre-check=0">
   <meta name="robots" content="noindex,nofollow">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <style type="text/css">
      body
      {
         background-color: #eee;
         padding: 20px;
         color: #555;
         font-family: Sans, Verdana, Arial, sans-serif;
      }
      
      h1
      {
         margin: 0;
         padding: 0;
      }
      
      a
      {
         color: #888;
         text-decoration: none;
         border-bottom: 1px solid #888;
      }
      
      a:hover
      {
         color: #aaa;
         text-decoration: none;
         border-bottom: 1px solid #888;
      }
   </style>
</head>
<body>
   <h1>Brak konfiguracji!</h1>
   <p>Watermelon CMS nie odnalazł pliku konfiguracji.</p>
   <p>Jeśli nie przeprowadziłeś jeszcze procesu instalacji, <a href="install.php">zrób to teraz</a></p>
</body>
</html>